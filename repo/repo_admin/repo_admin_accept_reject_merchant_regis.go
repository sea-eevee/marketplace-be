package repo_admin

const acceptRejectMerchantRegisQuery = "UPDATE merchant SET admin_approval = $1 WHERE id = $2"

func (r repoAdmin) AcceptOrRejectMerchantRegis(merchantID uint64, decision bool) error {
	_, err := r.db.Exec(acceptRejectMerchantRegisQuery, decision, merchantID)
	if err != nil {
		return err
	}
	return nil
}
