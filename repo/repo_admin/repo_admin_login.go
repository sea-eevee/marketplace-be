package repo_admin

import "gitlab.com/sea-eevee/marketplace-be/model"

func (r repoAdmin) Login(username string) (*model.Admin, error) {
	const query = "SELECT username, hash_password FROM admin WHERE username=$1;"

	rows, err := r.db.Query(query, username)
	if err != nil {
		return nil, err
	}
	admin := new(model.Admin)
	for rows.Next() {
		err = rows.Scan(&admin.Username, &admin.Password)
		if err != nil {
			return nil, err
		}
	}
	return admin, nil
}
