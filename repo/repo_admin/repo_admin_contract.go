package repo_admin

import (
	"database/sql"
	"gitlab.com/sea-eevee/marketplace-be/common/store"
	"gitlab.com/sea-eevee/marketplace-be/model"
)

//go:generate mockgen -destination=mock/mock.go -package=mock_repo_admin . Contract

type Contract interface {
	Register(username, password string) (insertedID uint64, err error)
	AcceptOrRejectMerchantRegis(merchantID uint64, decision bool) error
	IsAdminTokenExist(regisToken string) (bool, error)
	Login(username string) (*model.Admin, error)
}

type repoAdmin struct {
	db *sql.DB
}

func NewRepoAdmin(dbHost, dbUser, dbPass, dbName string) (Contract, error) {
	db, err := store.NewPostgres(dbHost, dbUser, dbPass, dbName)
	if err != nil {
		return nil, err
	}
	return repoAdmin{
		db: db,
	}, nil
}
