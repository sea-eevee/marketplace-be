package repo_admin

const queryIsAdminTokenExists = `
	SELECT COUNT(id)
	FROM admin_token
	WHERE token = $1
`

func (r repoAdmin) IsAdminTokenExist(regisToken string) (bool, error) {
	var count int
	row := r.db.QueryRow(queryIsAdminTokenExists, regisToken)
	err := row.Scan(&count)
	if err != nil {
		return false, err
	}

	return count == 1, nil
}
