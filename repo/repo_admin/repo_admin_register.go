package repo_admin

const queryRegister = "INSERT INTO admin(username, hash_password) VALUES ($1, $2) RETURNING id"

func (r repoAdmin) Register(username, password string) (insertedID uint64, err error) {
	err = r.db.QueryRow(queryRegister, username, password).Scan(&insertedID)
	if err != nil {
		return 0, err
	}
	return insertedID, err
}
