package repo_customer

import (
	"database/sql"

	_ "github.com/lib/pq"
	"gitlab.com/sea-eevee/marketplace-be/common/store"
	"gitlab.com/sea-eevee/marketplace-be/model"
)

//go:generate mockgen -destination=mock/mock.go -package=mock_repo_customer . Contract

type Contract interface {
	GetByUsername(username string) (*model.Customer, error)
	Create(username, password, email string) (insertedID int, err error)
	UpdateProfile(updated *model.Customer) error
	AddToCart(userID, itemID uint64) error
	GetShoppingCart(customerID uint64) ([]*model.Item, error)
	GetTransactionHistory(customerID uint64) ([]*model.Transaction, error)
}

type repoCustomer struct {
	db *sql.DB
}

func NewRepoCustomer(dbHost, dbUser, dbPass, dbName string) (Contract, error) {
	db, err := store.NewPostgres(dbHost, dbUser, dbPass, dbName)
	if err != nil {
		return nil, err
	}
	return &repoCustomer{
		db: db,
	}, nil
}
