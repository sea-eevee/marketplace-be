package repo_customer

func (r repoCustomer) Create(username, password, email string) (insertedID int, err error) {
	var lastInsertId int

	query := "INSERT INTO customer(username, hash_password, email) VALUES($1,$2,$3) returning id;"
	err = r.db.QueryRow(query, username, password, email).Scan(&lastInsertId)
	if err != nil {
		return 0, err
	}
	return insertedID, nil
}
