package repo_customer

import (
	"gitlab.com/sea-eevee/marketplace-be/model"
)

func (r repoCustomer) GetByUsername(username string) (*model.Customer, error) {
	rows, err := r.db.Query("SELECT id, username, hash_password FROM customer WHERE username = $1 LIMIT 1", username)
	if err != nil {
		return nil, err
	}

	cust := new(model.Customer)
	for rows.Next() {
		err = rows.Scan(&cust.ID, &cust.Username, &cust.Password)
		if err != nil {
			return nil, err
		}
	}
	return cust, nil
}
