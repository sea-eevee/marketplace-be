package repo_customer

import (
	"database/sql"
	"gitlab.com/sea-eevee/marketplace-be/model"
)

const queryGetShoppingCart = `
	SELECT i.id, merchant_id, item_type, stock, name, price, description
	FROM item i JOIN customer_shopping_cart csc on i.id = csc.item_id
	WHERE is_sold = false
`

func (r repoCustomer) GetShoppingCart(customerID uint64) ([]*model.Item, error) {
	var ts []*model.Item

	rows, err := r.db.Query(queryGetShoppingCart, customerID)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}
	for rows.Next() {
		t := new(model.Item)
		err := rows.Scan(t.ID, t.MerchantID, t.ItemType, t.Stock, t.Name, t.Price, t.Description)
		if err != nil {
			return nil, err
		}
		ts = append(ts, t)
	}

	return ts, nil
}
