package repo_customer

import "gitlab.com/sea-eevee/marketplace-be/model"

func (r repoCustomer) GetTransactionHistory(customerID uint64) ([]*model.Transaction, error) {
	panic("implement me")
}
