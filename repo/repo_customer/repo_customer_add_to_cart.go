package repo_customer

func (r repoCustomer) AddToCart(userID, itemID uint64) error {
	query := "INSERT INTO customer_shopping_cart(customer_id, item_id) VALUES($1,$2)"
	_, err := r.db.Exec(query, userID, itemID)
	if err != nil {
		return err
	}
	return nil
}
