// Code generated by MockGen. DO NOT EDIT.
// Source: gitlab.com/sea-eevee/marketplace-be/repo/repo_transaction (interfaces: Contract)

// Package mock_repo_transaction is a generated GoMock package.
package mock_repo_transaction

import (
	gomock "github.com/golang/mock/gomock"
	model "gitlab.com/sea-eevee/marketplace-be/model"
	reflect "reflect"
)

// MockContract is a mock of Contract interface
type MockContract struct {
	ctrl     *gomock.Controller
	recorder *MockContractMockRecorder
}

// MockContractMockRecorder is the mock recorder for MockContract
type MockContractMockRecorder struct {
	mock *MockContract
}

// NewMockContract creates a new mock instance
func NewMockContract(ctrl *gomock.Controller) *MockContract {
	mock := &MockContract{ctrl: ctrl}
	mock.recorder = &MockContractMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockContract) EXPECT() *MockContractMockRecorder {
	return m.recorder
}

// AcceptTransaction mocks base method
func (m *MockContract) AcceptTransaction(arg0, arg1, arg2, arg3, arg4 uint64) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "AcceptTransaction", arg0, arg1, arg2, arg3, arg4)
	ret0, _ := ret[0].(error)
	return ret0
}

// AcceptTransaction indicates an expected call of AcceptTransaction
func (mr *MockContractMockRecorder) AcceptTransaction(arg0, arg1, arg2, arg3, arg4 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "AcceptTransaction", reflect.TypeOf((*MockContract)(nil).AcceptTransaction), arg0, arg1, arg2, arg3, arg4)
}

// GetAllTransaction mocks base method
func (m *MockContract) GetAllTransaction() ([]*model.Transaction, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetAllTransaction")
	ret0, _ := ret[0].([]*model.Transaction)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetAllTransaction indicates an expected call of GetAllTransaction
func (mr *MockContractMockRecorder) GetAllTransaction() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetAllTransaction", reflect.TypeOf((*MockContract)(nil).GetAllTransaction))
}

// GetTransactionDetail mocks base method
func (m *MockContract) GetTransactionDetail(arg0 uint64) (*model.Transaction, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetTransactionDetail", arg0)
	ret0, _ := ret[0].(*model.Transaction)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetTransactionDetail indicates an expected call of GetTransactionDetail
func (mr *MockContractMockRecorder) GetTransactionDetail(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetTransactionDetail", reflect.TypeOf((*MockContract)(nil).GetTransactionDetail), arg0)
}

// PayTransaction mocks base method
func (m *MockContract) PayTransaction(arg0 uint64) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "PayTransaction", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// PayTransaction indicates an expected call of PayTransaction
func (mr *MockContractMockRecorder) PayTransaction(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "PayTransaction", reflect.TypeOf((*MockContract)(nil).PayTransaction), arg0)
}

// RejectTransaction mocks base method
func (m *MockContract) RejectTransaction(arg0, arg1 uint64) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "RejectTransaction", arg0, arg1)
	ret0, _ := ret[0].(error)
	return ret0
}

// RejectTransaction indicates an expected call of RejectTransaction
func (mr *MockContractMockRecorder) RejectTransaction(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RejectTransaction", reflect.TypeOf((*MockContract)(nil).RejectTransaction), arg0, arg1)
}
