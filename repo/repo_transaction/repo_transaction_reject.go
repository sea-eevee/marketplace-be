package repo_transaction

import "log"

const queryUpdateAdminApprovalReject = `
	UPDATE transaction 
	SET admin_approval = false
	WHERE id = $1
`

const queryUpdateBuyerTransactionHistoryFail = `
	INSERT INTO customer_transaction_history(customer_id, transaction_id, is_success) VALUES ($1, $2, false);
`

const queryDeleteItemFromShoppingCart = `
	DELETE FROM customer_transaction_history
	WHERE customer_id = $1 AND transaction_id = $2
`

func (r repoTransaction) RejectTransaction(transactionID, customerID uint64) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}

	_, err = tx.Exec(queryUpdateAdminApprovalReject, transactionID)
	if err != nil {
		tx.Rollback()
		log.Fatal(err)
	}

	_, err = tx.Exec(queryUpdateBuyerTransactionHistoryFail, customerID, transactionID)
	if err != nil {
		tx.Rollback()
		log.Fatal(err)
	}

	_, err = tx.Exec(queryDeleteItemFromShoppingCart, customerID, transactionID)
	if err != nil {
		tx.Rollback()
		log.Fatal(err)
	}

	err = tx.Commit()
	if err != nil {
		log.Fatal(err)
	}

	return nil
}
