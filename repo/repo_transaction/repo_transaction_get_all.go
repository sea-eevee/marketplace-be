package repo_transaction

import (
	"database/sql"
	"gitlab.com/sea-eevee/marketplace-be/model"
)

const queryGetAllTransaction = `
	SELECT id, item_id, customer_id, merchant_id, is_paid, admin_approval 
	FROM transaction
	WHERE is_paid IS NOT NULL
	`

func (r repoTransaction) GetAllTransaction() ([]*model.Transaction, error) {
	var ts []*model.Transaction

	rows, err := r.db.Query(queryGetAllTransaction)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}
	for rows.Next() {
		t := new(model.Transaction)
		err := rows.Scan(t.ID, t.Item.ID, t.Customer.ID, t.Merchant.ID, t.IsPaid, t.IsAccepted)
		if err != nil {
			return nil, err
		}
		ts = append(ts, t)
	}

	return ts, nil
}
