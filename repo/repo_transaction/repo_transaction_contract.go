package repo_transaction

import (
	"database/sql"
	"gitlab.com/sea-eevee/marketplace-be/common/store"
	"gitlab.com/sea-eevee/marketplace-be/model"
)

//go:generate mockgen -destination=mock/mock.go -package=mock_repo_transaction . Contract

type Contract interface {
	GetAllTransaction() ([]*model.Transaction, error)
	GetTransactionDetail(transactionID uint64) (*model.Transaction, error)
	PayTransaction(transactionID uint64) error
	AcceptTransaction(transactionID, merchantID, customerID, itemID, itemPrice uint64) error
	RejectTransaction(transactionID, customerID uint64) error
}

type repoTransaction struct {
	db *sql.DB
}

func NewRepoTransaction(dbHost, dbUser, dbPass, dbName string) (Contract, error) {
	db, err := store.NewPostgres(dbHost, dbUser, dbPass, dbName)
	if err != nil {
		return nil, err
	}
	return &repoTransaction{
		db: db,
	}, nil
}
