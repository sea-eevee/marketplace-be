package repo_transaction

import "gitlab.com/sea-eevee/marketplace-be/model"

const queryGetTransactionDetail = `
	SELECT id, item_id, customer_id, merchant_id, is_paid, admin_approval 
	FROM transaction 
	WHERE id = $1 LIMIT 1`

func (r repoTransaction) GetTransactionDetail(transactionID uint64) (*model.Transaction, error) {
	t := new(model.Transaction)
	row := r.db.QueryRow(queryGetTransactionDetail, transactionID)
	err := row.Scan(t.ID, t.Item.ID, t.Customer.ID, t.Merchant.ID, t.IsPaid, t.IsAccepted)
	if err != nil {
		return nil, err
	}
	return t, nil
}
