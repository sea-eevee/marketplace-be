package repo_transaction

const queryPayTransaction = `
	UPDATE transaction 
	SET is_paid = true
	WHERE id = $1
`

func (r repoTransaction) PayTransaction(transactionID uint64) error {
	_, err := r.db.Exec(queryPayTransaction, transactionID)
	if err != nil {
		return err
	}
	return nil
}
