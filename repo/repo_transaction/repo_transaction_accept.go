package repo_transaction

import "log"

const queryUpdateMerchantRealWallet = `
	UPDATE merchant
	SET real_wallet = $2
	WHERE merchant.id == $1
`

const queryUpdateBuyerTransactionHistorySuccess = `
	INSERT INTO customer_transaction_history(customer_id, transaction_id, is_success) VALUES ($1, $2, true);
`

const queryUpdateAdminApprovalAccept = `
	UPDATE transaction 
	SET admin_approval = true
	WHERE id = $1
`

const queryAddToMerchantRequestItem = `
	INSERT INTO merchant_request_item(merchant_id, item_id) 
	VALUES ($1, $2)
`

const queryUpdateItemToSold = `
	UPDATE item
	SET is_sold = true
	WHERE item.id = $1
`

func (r repoTransaction) AcceptTransaction(transactionID, merchantID, customerID, itemID, itemPrice uint64) error {
	// THEN the system should update the merchant real wallet according to the transaction
	// items, give success status in the buyer transaction history and add the items to the
	// request item menu in the merchant.

	tx, err := r.db.Begin()
	if err != nil {
		return err
	}

	_, err = tx.Exec(queryUpdateAdminApprovalAccept, transactionID)
	if err != nil {
		tx.Rollback()
		log.Fatal(err)
	}

	_, err = tx.Exec(queryUpdateMerchantRealWallet, merchantID, itemPrice)
	if err != nil {
		tx.Rollback()
		log.Fatal(err)
	}

	_, err = tx.Exec(queryUpdateBuyerTransactionHistorySuccess, customerID, transactionID)
	if err != nil {
		tx.Rollback()
		log.Fatal(err)
	}

	_, err = tx.Exec(queryDeleteItemFromShoppingCart, customerID, transactionID)
	if err != nil {
		tx.Rollback()
		log.Fatal(err)
	}

	_, err = tx.Exec(queryAddToMerchantRequestItem, merchantID, itemID)
	if err != nil {
		tx.Rollback()
		log.Fatal(err)
	}

	_, err = tx.Exec(queryUpdateItemToSold, itemID)
	if err != nil {
		tx.Rollback()
		log.Fatal(err)
	}

	err = tx.Commit()
	if err != nil {
		log.Fatal(err)
	}

	return nil
}
