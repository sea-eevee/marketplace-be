package repo_merchant

import (
	"gitlab.com/sea-eevee/marketplace-be/model"
)

func (r repoMerchant) Login(username string) (*model.Merchant, error) {
	const query = "SELECT id, username, hash_password, email, admin_approval, real_wallet FROM merchant WHERE username=$1;"
	rows, err := r.db.Query(query, username)
	if err != nil {
		return nil, err
	}
	merchant := new(model.Merchant)
	for rows.Next() {
		err = rows.Scan(&merchant.ID, &merchant.Username, &merchant.Password, &merchant.Email, &merchant.AdminApproval, &merchant.RealWallet)
		if err != nil {
			return nil, err
		}
	}
	return merchant, nil
}
