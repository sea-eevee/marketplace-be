package repo_merchant

import (
	"database/sql"
	"gitlab.com/sea-eevee/marketplace-be/model"
)

const queryGetMerchantItems = `
	SELECT id, item_type, name, price, description 
	FROM item 
	WHERE merchant_id = $1 AND is_sold = false
`

func (r repoMerchant) GetMerchantItems(merchantID uint64) ([]*model.Item, error) {
	var ts []*model.Item

	rows, err := r.db.Query(queryGetMerchantItems, merchantID)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}
	for rows.Next() {
		t := new(model.Item)
		err := rows.Scan(t.ID, t.ItemType, t.Stock, t.Price, t.Description)
		if err != nil {
			return nil, err
		}
		ts = append(ts, t)
	}

	return ts, nil
}
