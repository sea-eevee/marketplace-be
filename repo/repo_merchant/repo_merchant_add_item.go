package repo_merchant

import (
	"gitlab.com/sea-eevee/marketplace-be/model"
)

func (r repoMerchant) AddItem(merchantID int, item *model.Item) (insertedID int, err error) {
	var lastInsertId int
	query := "INSERT INTO item(merchant_id, item_type, stock, name, price, description) VALUES($1,$2, $3, $4, $5, $6) returning id;"
	err = r.db.QueryRow(query, merchantID, item.ItemType, item.Stock, item.Name, item.Price, item.Description).Scan(&lastInsertId)
	if err != nil {
		return 0, err
	}
	return lastInsertId, nil
}
