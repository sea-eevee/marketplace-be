package repo_merchant

import (
	"database/sql"
	"gitlab.com/sea-eevee/marketplace-be/model"
)

const queryGetAllMerchantDisplay = `
	SELECT id, username, display_name
	FROM merchant m JOIN merchant_profile mp on m.id = mp.merchant_id
	WHERE admin_approval = true
`

func (r repoMerchant) GetAllMerchantDisplay() ([]*model.Merchant, error) {
	var ts []*model.Merchant

	rows, err := r.db.Query(queryGetAllMerchantDisplay)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}
	for rows.Next() {
		m := new(model.Merchant)
		err := rows.Scan(m.ID, m.Username, m.DisplayName)
		if err != nil {
			return nil, err
		}
		ts = append(ts, m)
	}

	return ts, nil
}
