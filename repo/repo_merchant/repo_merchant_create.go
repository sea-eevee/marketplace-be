package repo_merchant

import (
	"gitlab.com/sea-eevee/marketplace-be/common/payload"
)

func (r repoMerchant) Create(data *payload.RegisterRequest) (insertedID int, err error) {
	var lastInsertId int
	query := "INSERT INTO merchant(username, hash_password, email, admin_approval) VALUES($1,$2, $3, $4) returning id;"
	err = r.db.QueryRow(query, data.Username, data.Password, data.Email, false).Scan(&lastInsertId)
	if err != nil {
		return 0, err
	}
	return lastInsertId, nil
}
