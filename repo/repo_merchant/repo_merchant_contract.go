package repo_merchant

import (
	"database/sql"

	"gitlab.com/sea-eevee/marketplace-be/model"

	"gitlab.com/sea-eevee/marketplace-be/common/payload"

	"gitlab.com/sea-eevee/marketplace-be/common/store"
)

//go:generate mockgen -destination=mock/mock.go -package=mock_repo_merchant . Contract

type Contract interface {
	Create(data *payload.RegisterRequest) (insertedID int, err error)
	Login(username string) (*model.Merchant, error)
	AddItem(merchantID int, item *model.Item) (insertedID int, err error)
	GetAllMerchantDisplay() ([]*model.Merchant, error)
	GetMerchantItems(merchantID uint64) ([]*model.Item, error)
}

type repoMerchant struct {
	db *sql.DB
}

func NewRepoMerchant(dbHost, dbUser, dbPass, dbName string) (Contract, error) {
	db, err := store.NewPostgres(dbHost, dbUser, dbPass, dbName)
	if err != nil {
		return nil, err
	}
	return &repoMerchant{
		db: db,
	}, nil
}
