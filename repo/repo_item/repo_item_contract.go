package repo_item

import (
	"database/sql"
	"gitlab.com/sea-eevee/marketplace-be/common/store"
	"gitlab.com/sea-eevee/marketplace-be/model"
)

//go:generate mockgen -destination=mock/mock.go -package=mock_repo_item . Contract

type Contract interface {
	GetItemDetail(itemID uint64) (*model.Item, error)
}

type repoItem struct {
	db *sql.DB
}

func NewRepoItem(dbHost, dbUser, dbPass, dbName string) (Contract, error) {
	db, err := store.NewPostgres(dbHost, dbUser, dbPass, dbName)
	if err != nil {
		return nil, err
	}
	return &repoItem{
		db: db,
	}, nil
}
