package repo_item

import "gitlab.com/sea-eevee/marketplace-be/model"

const queryGetItemDetail = `
	SELECT id, merchant_id, item_type, stock, name, price, description
	FROM item 
	WHERE id = $1 LIMIT 1`

func (r repoItem) GetItemDetail(itemID uint64) (*model.Item, error) {
	t := new(model.Item)
	row := r.db.QueryRow(queryGetItemDetail, itemID)
	err := row.Scan(t.ID, t.MerchantID, t.ItemType, t.Stock, t.Name, t.Price, t.Description)
	if err != nil {
		return nil, err
	}
	return t, nil
}
