package model

type Transfer struct {
	ID                uint64
	TransactionID     uint64
	SenderBankName    string
	SenderBankAccount string
}
