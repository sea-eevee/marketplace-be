package model

type Customer struct {
	ID                 uint64
	Username           string
	Password           string
	DisplayName        string
	ShoppingCart       []Item
	TransactionHistory []Transaction
}
