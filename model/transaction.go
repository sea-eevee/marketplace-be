package model

import "time"

type Transaction struct {
	ID         uint64
	Customer   *Customer
	Item       *Item
	Merchant   *Merchant
	IsAccepted *bool
	CreatedAt  *time.Time
	IsPaid     *bool
}
