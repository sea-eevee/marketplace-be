package model

type Merchant struct {
	ID              uint64
	Username        string
	Email           string
	Password        string
	DisplayName     string
	Description     string
	Items           []Item
	RealWallet      uint64
	BankName        string
	BankAccount     string
	TransferHistory []Transfer
	AdminApproval   bool
}
