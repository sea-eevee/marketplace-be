package model

type Admin struct {
	ID                uint64
	RegistrationToken string
	Username          string
	Password          string
}
