package model

type Item struct {
	ID          uint64 `json:"id"`
	MerchantID  uint64 `json:"merchant_id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Price       uint64 `json:"price"`
	Stock       uint64 `json:"stock"`
	ItemType    uint64 `json:"item_type"`
}
