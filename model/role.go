package model

const (
	RoleAdmin    = "admin"
	RoleCustomer = "customer"
	RoleMerchant = "merchant"
)
