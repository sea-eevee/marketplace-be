module gitlab.com/sea-eevee/marketplace-be

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/golang/mock v1.4.3
	github.com/gorilla/handlers v1.5.0
	github.com/gorilla/mux v1.7.4
	github.com/lib/pq v1.8.0
	github.com/myesui/uuid v1.0.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/rs/zerolog v1.18.0
	github.com/spf13/viper v1.7.0
	github.com/stretchr/testify v1.3.0
	github.com/twinj/uuid v1.0.0
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550
	golang.org/x/sys v0.0.0-20200116001909-b77594299b42 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/stretchr/testify.v1 v1.2.2 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
