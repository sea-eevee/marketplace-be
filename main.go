package main

import (
	"gitlab.com/sea-eevee/marketplace-be/repo/repo_item"
	"log"
	"net/http"

	"gitlab.com/sea-eevee/marketplace-be/repo/repo_transaction"

	"github.com/gorilla/handlers"

	"gitlab.com/sea-eevee/marketplace-be/common/config"
	"gitlab.com/sea-eevee/marketplace-be/common/token"
	"gitlab.com/sea-eevee/marketplace-be/repo/repo_admin"
	"gitlab.com/sea-eevee/marketplace-be/repo/repo_customer"
	"gitlab.com/sea-eevee/marketplace-be/repo/repo_merchant"
	"gitlab.com/sea-eevee/marketplace-be/usecase/uc_admin"
	"gitlab.com/sea-eevee/marketplace-be/usecase/uc_customer"
	"gitlab.com/sea-eevee/marketplace-be/usecase/uc_merchant"
)

type domain struct {
	ucCustomer uc_customer.Contract
	ucMerchant uc_merchant.Contract
	ucAdmin    uc_admin.Contract
}

func main() {
	cfg := config.InitAppConfig()
	tokenizer := token.NewTokenizer(cfg.Secret, int64(cfg.JwtAccessExpires), int64(cfg.JwtRefreshExpires))

	repoCustomer, err := repo_customer.NewRepoCustomer(cfg.DBHost, cfg.DBUser, cfg.DBPass, cfg.DBName)
	if err != nil {
		log.Fatal(err)
	}

	repoMerchant, err := repo_merchant.NewRepoMerchant(cfg.DBHost, cfg.DBUser, cfg.DBPass, cfg.DBName)
	if err != nil {
		log.Fatal(err)
	}

	repoTransaction, err := repo_transaction.NewRepoTransaction(cfg.DBHost, cfg.DBUser, cfg.DBPass, cfg.DBName)
	if err != nil {
		log.Fatal(err)
	}

	repoAdmin, err := repo_admin.NewRepoAdmin(cfg.DBHost, cfg.DBUser, cfg.DBPass, cfg.DBName)
	if err != nil {
		log.Fatal(err)
	}

	repoItem, err := repo_item.NewRepoItem(cfg.DBHost, cfg.DBUser, cfg.DBPass, cfg.DBName)
	if err != nil {
		log.Fatal(err)
	}

	ucCustomer := uc_customer.NewUCCustomer(repoCustomer, repoMerchant, tokenizer)
	ucMerchant := uc_merchant.NewUCMerchant(repoMerchant, tokenizer)
	ucAdmin := uc_admin.NewUCAdmin(uc_admin.UCAdminParam{
		RepoAdmin:       repoAdmin,
		RepoMerchant:    repoMerchant,
		RepoCustomer:    repoCustomer,
		RepoTransaction: repoTransaction,
		RepoItem:        repoItem,
		Tokenizer:       tokenizer,
	})

	r := NewRouter(domain{
		ucCustomer: ucCustomer,
		ucMerchant: ucMerchant,
		ucAdmin:    ucAdmin,
	}, tokenizer)

	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})

	err = http.ListenAndServe(":8080", handlers.CORS(originsOk, headersOk, methodsOk)(r))
	if err != nil {
		panic(err)
	}
}
