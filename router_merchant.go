package main

import (
	"net/http"

	"gitlab.com/sea-eevee/marketplace-be/common/token"

	"gitlab.com/sea-eevee/marketplace-be/api/api_merchant"

	"github.com/gorilla/mux"
	"gitlab.com/sea-eevee/marketplace-be/usecase/uc_merchant"
)

func ApplyRouterMerchant(r *mux.Router, ucMerchant uc_merchant.Contract, tokenizer token.Contract) *mux.Router {

	merchant := r.PathPrefix("/merchant").Subrouter()
	{
		merchant.
			Methods(http.MethodPost).
			Path("/register").
			Handler(api_merchant.Register(ucMerchant.Register))

		merchant.
			Methods(http.MethodGet).
			Path("/login").
			Handler(api_merchant.Login(ucMerchant.Login))

		merchant.
			Methods(http.MethodPost).
			Path("/item").
			Handler(
				tokenizer.MiddlewareAuth(
					api_merchant.AddItem(ucMerchant.AddItem),
				),
			)

	}
	return merchant
}
