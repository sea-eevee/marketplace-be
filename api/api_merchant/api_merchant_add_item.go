package api_merchant

import (
	"encoding/json"
	"net/http"
	"strconv"

	"gitlab.com/sea-eevee/marketplace-be/model"

	"gitlab.com/sea-eevee/marketplace-be/common/errs"
	"gitlab.com/sea-eevee/marketplace-be/common/responder"
	"gitlab.com/sea-eevee/marketplace-be/usecase/uc_merchant"
)

func AddItem(addItemUCFunc uc_merchant.AddItemUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		merchantID, err := strconv.Atoi(r.Header.Get(model.KeyHeaderUserID))
		if err != nil {
			responder.ResponseError(w, err)
			return
		}
		item := &model.Item{}
		if err := json.NewDecoder(r.Body).Decode(&item); err != nil {
			responder.ResponseError(w, errs.ErrBadRequest)
			return
		}
		ts, err := addItemUCFunc(merchantID, item)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, ts)
	})
}
