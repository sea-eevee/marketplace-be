package api_merchant

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"gitlab.com/sea-eevee/marketplace-be/usecase/uc_merchant"

	"gitlab.com/sea-eevee/marketplace-be/common/payload"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/sea-eevee/marketplace-be/model"
)

func TestRegister(t *testing.T) {
	sampleUsername := "fdajsjfa"
	samplePassword := "fjuajfew"
	sampleEmail := "abc"
	p := payload.RegisterRequest{
		Email:    sampleEmail,
		Password: samplePassword,
		Username: sampleUsername,
	}
	sampleBody, err := json.Marshal(p)
	if err != nil {
		log.Fatal(err)
	}

	var tts = []struct {
		caseName    string
		handlerFunc uc_merchant.RegisterUCFunc
		request     func() *http.Request
		result      func(resp *http.Response)
	}{
		{
			caseName: "when everything is all right should return 200 OK",
			handlerFunc: func(param *payload.RegisterRequest) (*payload.RegisterResponse, error) {
				assert.Equal(t, sampleUsername, param.Username)
				assert.Equal(t, samplePassword, param.Password)
				return &payload.RegisterResponse{Token: &model.TokenDetails{}}, nil
			},
			request: func() *http.Request {
				r, _ := http.NewRequest(http.MethodPost, "/merchant/register", bytes.NewBuffer(sampleBody))
				return r
			},
			result: func(resp *http.Response) {
				assert.Equal(t, http.StatusOK, resp.StatusCode)
			},
		},
	}

	for _, tt := range tts {
		t.Log(tt.caseName)

		router := mux.NewRouter()

		router.Path("/merchant/register").Handler(Register(tt.handlerFunc))

		rr := httptest.NewRecorder()

		req := tt.request()
		req.AddCookie(&http.Cookie{
			Name:     "TOKEN",
			Value:    "authorization info",
			Path:     "/",
			Expires:  time.Now().Add(1 * time.Hour),
			HttpOnly: true,
		})

		router.ServeHTTP(rr, req)

		tt.result(rr.Result())
	}
}
