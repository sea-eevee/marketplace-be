package api_merchant

import (
	"encoding/json"
	"net/http"

	"gitlab.com/sea-eevee/marketplace-be/common/errs"

	"gitlab.com/sea-eevee/marketplace-be/common/payload"
	"gitlab.com/sea-eevee/marketplace-be/common/responder"

	"gitlab.com/sea-eevee/marketplace-be/usecase/uc_merchant"
)

func Register(registerUCFunc uc_merchant.RegisterUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		params := &payload.RegisterRequest{}
		err := json.NewDecoder(r.Body).Decode(&params)
		if err != nil {
			responder.ResponseError(w, errs.ErrBadRequest)
			return
		}

		ts, err := registerUCFunc(params)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, ts)

	})
}
