package api_customer

import (
	"gitlab.com/sea-eevee/marketplace-be/common/request_getter"
	"gitlab.com/sea-eevee/marketplace-be/common/responder"
	"gitlab.com/sea-eevee/marketplace-be/usecase/uc_customer"
	"net/http"
)

func SeeProfile(ucFunc uc_customer.SeeProfileUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		uid, err := request_getter.ExtractUserID(r)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		ts, err := ucFunc(&uc_customer.SeeProfileParam{CustomerID: uid})
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, ts)
	})
}
