package api_customer

import (
	"encoding/json"
	"gitlab.com/sea-eevee/marketplace-be/common/errs"
	"gitlab.com/sea-eevee/marketplace-be/common/responder"
	"gitlab.com/sea-eevee/marketplace-be/usecase/uc_customer"
	"net/http"
)

func Login(ucFunc uc_customer.LoginUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		params := new(uc_customer.LoginParam)
		err := json.NewDecoder(r.Body).Decode(params)
		if err != nil {
			responder.ResponseError(w, errs.ErrBadRequest)
			return
		}

		ts, err := ucFunc(params)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, ts)
	})
}
