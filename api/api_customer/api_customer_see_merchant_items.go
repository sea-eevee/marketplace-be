package api_customer

import (
	"github.com/gorilla/mux"
	"gitlab.com/sea-eevee/marketplace-be/common/responder"
	"gitlab.com/sea-eevee/marketplace-be/usecase/uc_customer"
	"net/http"
	"strconv"
)

const URLKeyMerchantID = "merchant_id"

func SeeMerchantItems(ucFunc uc_customer.SeeMerchantItemsUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		vars := mux.Vars(r)[URLKeyMerchantID]
		merchantID, err := strconv.ParseUint(vars, 10, 64)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		ts, err := ucFunc(&uc_customer.SeeMerchantItemsParam{MerchantID: merchantID})
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, ts)
	})
}
