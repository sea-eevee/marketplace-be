package api_customer

import (
	"gitlab.com/sea-eevee/marketplace-be/common/request_getter"
	"gitlab.com/sea-eevee/marketplace-be/common/responder"
	"gitlab.com/sea-eevee/marketplace-be/usecase/uc_customer"
	"net/http"
)

func SeeTransactionHistory(ucFunc uc_customer.SeeTransactionHistory) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		uid, err := request_getter.ExtractUserID(r)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		ts, err := ucFunc(&uc_customer.SeeTransactionHistoryParam{CustomerID: uid})
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, ts)
	})
}
