package api_admin

import (
	"github.com/gorilla/mux"
	"gitlab.com/sea-eevee/marketplace-be/common/responder"
	"gitlab.com/sea-eevee/marketplace-be/usecase/uc_admin"
	"net/http"
	"strconv"
)

const URLKeyTransactionID = "transaction_id"

func AcceptTransaction(ucFunc uc_admin.AcceptTransactionUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)[URLKeyTransactionID]
		transactionID, err := strconv.ParseUint(vars, 10, 64)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		resp, err := ucFunc(&uc_admin.AcceptTransactionParam{TransactionID: transactionID})
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, resp)
	})
}
