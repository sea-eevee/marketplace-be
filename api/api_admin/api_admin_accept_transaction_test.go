package api_admin

import (
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/sea-eevee/marketplace-be/usecase/uc_admin"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestAcceptTransaction(t *testing.T) {
	sampleTransactionID := uint64(5)

	var tts = []struct {
		caseName    string
		handlerFunc uc_admin.AcceptTransactionUCFunc
		request     func() *http.Request
		result      func(resp *http.Response)
	}{
		{
			caseName: "when everything is all right should return 200 OK",
			request: func() *http.Request {
				r, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("/%d", sampleTransactionID), nil)
				return r
			},
			handlerFunc: func(param *uc_admin.AcceptTransactionParam) (*uc_admin.AcceptTransactionResponse, error) {
				assert.Equal(t, sampleTransactionID, param.TransactionID)
				return &uc_admin.AcceptTransactionResponse{}, nil
			},
			result: func(resp *http.Response) {
				assert.Equal(t, http.StatusOK, resp.StatusCode)
			},
		},
		{
			caseName: "when handlerFunc err return err",
			request: func() *http.Request {
				r, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("/%d", sampleTransactionID), nil)
				return r
			},
			handlerFunc: func(param *uc_admin.AcceptTransactionParam) (*uc_admin.AcceptTransactionResponse, error) {
				return &uc_admin.AcceptTransactionResponse{}, errors.New("any error")
			},
			result: func(resp *http.Response) {
				assert.NotEqual(t, resp.StatusCode, http.StatusOK)
			},
		},
	}

	for _, tt := range tts {
		t.Log(tt.caseName)

		router := mux.NewRouter()

		router.Path(fmt.Sprintf("/{%s:[0-9]+}", URLKeyTransactionID)).Handler(AcceptTransaction(tt.handlerFunc))

		rr := httptest.NewRecorder()

		req := tt.request()
		router.ServeHTTP(rr, req)

		tt.result(rr.Result())
	}
}
