package api_admin

import (
	"encoding/json"
	"gitlab.com/sea-eevee/marketplace-be/common/errs"
	"gitlab.com/sea-eevee/marketplace-be/common/responder"
	"gitlab.com/sea-eevee/marketplace-be/usecase/uc_admin"
	"net/http"
)

func Register(ucFunc uc_admin.RegisterUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		param := new(uc_admin.RegisterParam)
		err := json.NewDecoder(r.Body).Decode(param)
		if err != nil {
			responder.ResponseError(w, errs.ErrBadRequest)
			return
		}

		resp, err := ucFunc(&uc_admin.RegisterParam{
			RegistrationToken: param.RegistrationToken,
			Username:          param.Username,
			Password:          param.Password,
		})
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, resp)
	})
}
