package api_admin

import (
	"encoding/json"
	"gitlab.com/sea-eevee/marketplace-be/common/errs"
	"gitlab.com/sea-eevee/marketplace-be/common/responder"
	"gitlab.com/sea-eevee/marketplace-be/usecase/uc_admin"
	"net/http"
)

func Login(ucFunc uc_admin.LoginUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		param := new(uc_admin.LoginParam)
		err := json.NewDecoder(r.Body).Decode(param)
		if err != nil {
			responder.ResponseError(w, errs.ErrBadRequest)
			return
		}

		resp, err := ucFunc(&uc_admin.LoginParam{
			Username: param.Username,
			Password: param.Password,
		})
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, resp)
	})
}
