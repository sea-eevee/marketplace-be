package api_admin

import (
	"github.com/gorilla/mux"
	"gitlab.com/sea-eevee/marketplace-be/common/responder"
	"gitlab.com/sea-eevee/marketplace-be/usecase/uc_admin"
	"net/http"
	"strconv"
)

func RejectTransaction(ucFunc uc_admin.RejectTransactionUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)[URLKeyTransactionID]
		transactionID, err := strconv.ParseUint(vars, 10, 64)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		resp, err := ucFunc(&uc_admin.RejectTransactionParam{TransactionID: transactionID})
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, resp)
	})
}
