package api_admin

import (
	"bytes"
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/sea-eevee/marketplace-be/model"
	"gitlab.com/sea-eevee/marketplace-be/usecase/uc_admin"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestRegister(t *testing.T) {
	sampleToken := "fja329djq92asjf9"
	sampleUsername := "fdajsjfa"
	samplePassword := "fjuajfew"
	p := uc_admin.RegisterParam{
		RegistrationToken: sampleToken,
		Username:          sampleUsername,
		Password:          samplePassword,
	}
	sampleBody, err := json.Marshal(p)
	if err != nil {
		log.Fatal(err)
	}

	var tts = []struct {
		caseName    string
		handlerFunc uc_admin.RegisterUCFunc
		request     func() *http.Request
		result      func(resp *http.Response)
	}{
		{
			caseName: "when everything is all right should return 200 OK",
			handlerFunc: func(param *uc_admin.RegisterParam) (*uc_admin.RegisterResponse, error) {
				assert.Equal(t, sampleToken, param.RegistrationToken)
				assert.Equal(t, sampleUsername, param.Username)
				assert.Equal(t, samplePassword, param.Password)
				return &uc_admin.RegisterResponse{Token: &model.TokenDetails{}}, nil
			},
			request: func() *http.Request {
				r, _ := http.NewRequest(http.MethodPost, "/", bytes.NewBuffer(sampleBody))
				return r
			},
			result: func(resp *http.Response) {
				assert.Equal(t, http.StatusOK, resp.StatusCode)
			},
		},
	}

	for _, tt := range tts {
		t.Log(tt.caseName)

		router := mux.NewRouter()

		router.Path("/").Handler(Register(tt.handlerFunc))

		rr := httptest.NewRecorder()

		req := tt.request()
		req.AddCookie(&http.Cookie{
			Name:     "TOKEN",
			Value:    "authorization info",
			Path:     "/",
			Expires:  time.Now().Add(1 * time.Hour),
			HttpOnly: true,
		})

		router.ServeHTTP(rr, req)

		tt.result(rr.Result())
	}
}
