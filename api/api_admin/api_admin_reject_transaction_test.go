package api_admin

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/sea-eevee/marketplace-be/usecase/uc_admin"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestRejectTransaction(t *testing.T) {
	sampleTransactionID := uint64(5)

	var tts = []struct {
		caseName    string
		handlerFunc uc_admin.RejectTransactionUCFunc
		request     func() *http.Request
		result      func(resp *http.Response)
	}{
		{
			caseName: "when everything is all right should return 200 OK",
			request: func() *http.Request {
				r, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("/%d", sampleTransactionID), nil)
				return r
			},
			handlerFunc: func(param *uc_admin.RejectTransactionParam) (*uc_admin.RejectTransactionResponse, error) {
				assert.Equal(t, sampleTransactionID, param.TransactionID)
				return &uc_admin.RejectTransactionResponse{}, nil
			},
			result: func(resp *http.Response) {
				assert.Equal(t, http.StatusOK, resp.StatusCode)
			},
		},
	}

	for _, tt := range tts {
		t.Log(tt.caseName)

		router := mux.NewRouter()

		router.Handle(fmt.Sprintf("/{%s:[0-9]+}", URLKeyTransactionID), RejectTransaction(tt.handlerFunc))

		rr := httptest.NewRecorder()

		req := tt.request()
		router.ServeHTTP(rr, req)

		tt.result(rr.Result())
	}
}
