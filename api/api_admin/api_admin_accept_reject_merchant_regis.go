package api_admin

import (
	"encoding/json"
	"gitlab.com/sea-eevee/marketplace-be/common/responder"
	"gitlab.com/sea-eevee/marketplace-be/usecase/uc_admin"
	"net/http"
)

func AcceptRejectMerchantRegis(ucFunc uc_admin.AcceptRejectMerchantRegisUCFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		param := new(uc_admin.AcceptRejectMerchantRegisParam)
		err := json.NewDecoder(r.Body).Decode(param)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		resp, err := ucFunc(param)
		if err != nil {
			responder.ResponseError(w, err)
			return
		}

		responder.ResponseOK(w, resp)
	})
}
