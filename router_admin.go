package main

import (
	"fmt"
	"gitlab.com/sea-eevee/marketplace-be/common/token"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/sea-eevee/marketplace-be/api/api_admin"
	"gitlab.com/sea-eevee/marketplace-be/usecase/uc_admin"
)

func ApplyRouterAdmin(r *mux.Router, ucAdmin uc_admin.Contract, tokenizer token.Contract) *mux.Router {

	admin := r.PathPrefix("/admin").Subrouter()
	{
		admin.
			Methods(http.MethodPost).
			Path("/register").
			Handler(api_admin.Register(ucAdmin.Register))

		admin.
			Methods(http.MethodGet).
			Path("/login").
			Handler(api_admin.Login(ucAdmin.Login))

		admin.Methods(http.MethodPost).
			Path("/merchant/proposal").
			Handler(tokenizer.MiddlewareAuth(
				api_admin.AcceptRejectMerchantRegis(ucAdmin.AcceptOrRejectMerchantRegis),
			))

		admin.Methods(http.MethodPost).
			Path(fmt.Sprintf("transaction/{%s:[0-9]+}/accept", api_admin.URLKeyTransactionID)).
			Handler(tokenizer.MiddlewareAuth(
				api_admin.AcceptTransaction(ucAdmin.AcceptTransaction),
			))

		admin.Methods(http.MethodPost).
			Path(fmt.Sprintf("transaction/{%s:[0-9]+}/reject", api_admin.URLKeyTransactionID)).
			Handler(tokenizer.MiddlewareAuth(
				api_admin.RejectTransaction(ucAdmin.RejectTransaction),
			))

		admin.Methods(http.MethodPut)
	}
	return r
}
