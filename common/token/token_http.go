package token

import (
	"gitlab.com/sea-eevee/marketplace-be/common/errs"
	"gitlab.com/sea-eevee/marketplace-be/common/responder"
	"gitlab.com/sea-eevee/marketplace-be/model"
	"net/http"
	"strconv"
	"strings"
)

func extractToken(bearToken string) string {
	strArr := strings.Split(bearToken, " ")
	if len(strArr) == 2 {
		return strArr[1]
	}
	return ""
}

func (t *tokenizer) MiddlewareAuth(next http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		clientToken := extractToken(r.Header.Get("Authorization"))
		accessDetail, err := t.DecodeTokenDetails(clientToken)
		if err != nil {
			responder.ResponseError(w, errs.ErrAuth)
			return
		}

		r.Header.Set(model.KeyHeaderRole, accessDetail.Role)
		r.Header.Set(model.KeyHeaderUserID, strconv.FormatUint(accessDetail.UserId, 10))
		next.ServeHTTP(w, r)

	}
}
