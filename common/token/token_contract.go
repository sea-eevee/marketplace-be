package token

import (
	"gitlab.com/sea-eevee/marketplace-be/model"
	"net/http"
)

type Contract interface {
	CreateToken(role string, userid uint64) (*model.TokenDetails, error)
	DecodeTokenDetails(rawToken string) (*model.AccessDetails, error)
	MiddlewareAuth(next http.Handler) http.HandlerFunc
}

type tokenizer struct {
	secret    string
	atExpires int64
	rtExpires int64
}

func NewTokenizer(secret string, AtExpires, RtExpires int64) Contract {
	return &tokenizer{
		secret:    secret,
		atExpires: AtExpires,
		rtExpires: RtExpires,
	}
}
