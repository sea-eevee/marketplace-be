package token

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/twinj/uuid"
	"gitlab.com/sea-eevee/marketplace-be/model"
	"time"
)

func (t *tokenizer) CreateToken(role string, userid uint64) (*model.TokenDetails, error) {
	cfgAccessExp := time.Duration(t.atExpires)
	cfgRefreshExp := time.Duration(t.rtExpires)

	td := &model.TokenDetails{
		AtExpires:   time.Now().Add(time.Minute * cfgAccessExp).Unix(),
		RtExpires:   time.Now().Add(time.Hour * 24 * cfgRefreshExp).Unix(),
		AccessUuid:  uuid.NewV4().String(),
		RefreshUuid: uuid.NewV4().String(),
	}

	accessToken, err := createAccessToken(role, userid, td, t.secret)
	refreshToken, err := createRefreshToken(role, userid, td, t.secret)

	if err != nil {
		return nil, err
	}

	td.AccessToken = accessToken
	td.RefreshToken = refreshToken

	return td, nil
}

func createRefreshToken(role string, userid uint64, td *model.TokenDetails, secret string) (string, error) {
	rtClaims := jwt.MapClaims{
		"refresh_uuid": td.RefreshUuid,
		"role":         role,
		"user_id":      userid,
		"exp":          td.RtExpires,
	}
	rt := jwt.NewWithClaims(jwt.SigningMethodHS256, rtClaims)
	return rt.SignedString([]byte(secret))
}

func createAccessToken(role string, userid uint64, td *model.TokenDetails, secret string) (string, error) {
	atClaims := jwt.MapClaims{
		"access_uuid": td.AccessUuid,
		"role":        role,
		"user_id":     userid,
		"exp":         td.AtExpires,
		"authorized":  true,
	}
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	return at.SignedString([]byte(secret))
}
