package token

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/sea-eevee/marketplace-be/common/errs"
	"gitlab.com/sea-eevee/marketplace-be/model"
	"strconv"
)

func (t *tokenizer) DecodeTokenDetails(rawToken string) (*model.AccessDetails, error) {
	tokenParsed, err := jwt.Parse(rawToken, CheckConformHMAC(t.secret))
	if err != nil {
		return nil, err
	}
	claims, ok := tokenParsed.Claims.(jwt.MapClaims)
	if !ok || !tokenParsed.Valid {
		return nil, err
	}
	accessUuid, ok := claims["access_uuid"].(string)
	if !ok {
		return nil, err
	}
	role, ok := claims["role"].(string) //convert the interface to string
	if !ok {
		return nil, errs.ErrAuth
	}
	userId, err := strconv.ParseUint(fmt.Sprintf("%.f", claims["user_id"]), 10, 64)
	if err != nil {
		return nil, errs.ErrAuth
	}
	return &model.AccessDetails{
		AccessUuid: accessUuid,
		UserId:     userId,
		Role:       role,
	}, nil
}
