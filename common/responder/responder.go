package responder

import (
	"encoding/json"
	"errors"
	"gitlab.com/sea-eevee/marketplace-be/common/errs"
	"net/http"
)

func ResponseOK(w http.ResponseWriter, body interface{}) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	resp := CommonResponse{
		Status:      0,
		Description: "success",
		Data:        body,
	}

	return json.NewEncoder(w).Encode(resp)
}

func ResponseError(w http.ResponseWriter, err error) error {
	httpCode, resp := errToHttpFormat(err)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(httpCode)

	return json.NewEncoder(w).Encode(resp)
}

func checkErrorType(err error, target ...error) bool {
	for _, t := range target {
		if errors.Is(err, t) {
			return true
		}
	}

	return false
}

func errToHttpFormat(err error) (int, interface{}) {
	resp := CommonResponse{Status: 1, Description: err.Error()}

	switch {
	case checkErrorType(err, errs.ErrAuth):
		return http.StatusUnauthorized, resp
	case checkErrorType(err, errs.ErrBadRequest):
		return http.StatusUnauthorized, resp
	case checkErrorType(err, errs.ErrAdminTokenNotExists):
		return http.StatusBadRequest, resp
	case checkErrorType(err, errs.ErrUsernameExists):
		return http.StatusBadRequest, resp
	default:
		return http.StatusInternalServerError, resp
	}
}

type CommonResponse struct { // for consumer
	Status      int         `json:"status"`
	Description string      `json:"description"`
	Data        interface{} `json:"data,omitempty"`
}
