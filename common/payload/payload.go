package payload

import "gitlab.com/sea-eevee/marketplace-be/model"

type RegisterRequest struct {
	Username string
	Email    string
	Password string
}

type LoginRequest struct {
	Username string
	Password string
}

type RegisterResponse struct {
	Token *model.TokenDetails
}

type LoginResponse struct {
	Token *model.TokenDetails
	User  interface{}
}
