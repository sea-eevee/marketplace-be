package store

import (
	"database/sql"
	"fmt"
	"log"
)

func NewPostgres(dbHost, dbUser, dbPass, dbName string) (*sql.DB, error) {
	dbInfo := fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable",
		dbHost, dbUser, dbPass, dbName)
	log.Println(dbInfo)
	db, err := sql.Open("postgres", dbInfo)
	if err != nil {
		return nil, err
	}
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	log.Print("database connection success")
	return db, nil
}
