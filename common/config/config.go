package config

import (
	"fmt"
	"github.com/spf13/viper"
	"os"
)

type Config struct {

	// Token
	Secret string `mapstructure:"secret"`

	// db -> MySQL
	DBHost string `mapstructure:"db_host"`
	DBPort string `mapstructure:"db_port"`
	DBUser string `mapstructure:"db_user"`
	DBPass string `mapstructure:"db_password"`
	DBName string `mapstructure:"db_name"`

	// JWT
	JwtAccessExpires  int `mapstructure:"jwt_at_expire"`
	JwtRefreshExpires int `mapstructure:"jwt_rt_expire"`
}

func parseConfigFilePath() string {
	workPath, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	return workPath
}

var AppConfig *Config

func InitAppConfig() *Config {
	configPath := parseConfigFilePath()
	viper.SetConfigName("config")
	viper.SetConfigType("env")
	viper.AddConfigPath(configPath)
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	config := new(Config)
	if err := viper.Unmarshal(config); err != nil {
		panic(fmt.Errorf("failed to parse config file: %w", err))
	}

	return config
}
