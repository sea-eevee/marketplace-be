package request_getter

import (
	"gitlab.com/sea-eevee/marketplace-be/model"
	"net/http"
)

func ExtractRole(r *http.Request) (string, error) {

	roles := []string{model.RoleAdmin, model.RoleCustomer, model.RoleMerchant}

	role := r.Header.Get(model.KeyHeaderRole)
	for _, ctr := range roles {
		if role == ctr {
			return role, nil
		}
	}

	return role, nil
}
