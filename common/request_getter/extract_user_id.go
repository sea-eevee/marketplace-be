package request_getter

import (
	"errors"
	"gitlab.com/sea-eevee/marketplace-be/model"
	"net/http"
	"strconv"
)

func ExtractUserID(r *http.Request) (uint64, error) {
	idStr := r.Header.Get(model.KeyHeaderUserID)
	if idStr == "" {
		return 0, errors.New("todo")
	}

	id, err := strconv.ParseUint(idStr, 10, 64)
	if err != nil {
		return 0, err
	}

	return id, nil
}
