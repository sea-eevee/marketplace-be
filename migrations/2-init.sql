-- LOCATION
INSERT INTO location(province, city) VALUES ('DKI Jakarta', 'Jakarta Pusat');
INSERT INTO location(province, city) VALUES ('DKI Jakarta', 'Jakarta Selatan');
INSERT INTO location(province, city) VALUES ('Jawa Barat', 'Kota Depok');
INSERT INTO location(province, city) VALUES ('Jawa Barat', 'Kota Bogor');
INSERT INTO location(province, city) VALUES ('Banten', 'Kota Tangerang');
INSERT INTO location(province, city) VALUES ('Jawa Tengah', 'Kota Solo');
INSERT INTO location(province, city) VALUES ('Jawa Timur', 'Kota Surabaya');


-- ADMIN TOKEN
INSERT INTO admin_token(token) VALUES ('tokenku');
INSERT INTO admin_token(token) VALUES ('tokenmu');
INSERT INTO admin_token(token) VALUES ('tokendia');


-- ADMIN
INSERT INTO admin(username, hash_password) VALUES ('john.doe.admin', '$2a$10$4jWZ.8fORKmMuO3KJzWESube42zZHSbBWq7YtdxEbQNSIoZtW.18C');


-- CUSTOMER
INSERT INTO customer(id, username, hash_password, email) VALUES (1, 'john.doe.customer', '$2a$10$Cg8cg2eow3LLPoDfQ1nDTurbknT3NTArCeeuXe8km0OIXlYWSz5Ly', 'john.doe.customer@gmail.com');
INSERT INTO customer(id, username, hash_password, email) VALUES (2, 'madara.customer', '$2a$10$cEAiE.ry4Z6dgt/QVMQYNe8osRRTj56xXQPWe3v60.mvQQuyq02QC', 'madara.customer@gmail.com');
INSERT INTO customer(id, username, hash_password, email) VALUES (3, 'sasuke.customer', '$2a$10$cEAiE.ry4Z6dgt/QVMQYNe8osRRTj56xXQPWe3v60.mvQQuyq02QC', 'sasuke.customer@gmail.com');
INSERT INTO customer(id, username, hash_password, email) VALUES (4, 'naruto.customer', '$2a$10$cEAiE.ry4Z6dgt/QVMQYNe8osRRTj56xXQPWe3v60.mvQQuyq02QC', 'naruto.customer@gmail.com');
INSERT INTO customer(id, username, hash_password, email) VALUES (5, 'sanji.customer', '$2a$10$cEAiE.ry4Z6dgt/QVMQYNe8osRRTj56xXQPWe3v60.mvQQuyq02QC', 'sanji.customer@gmail.com');
INSERT INTO customer(id, username, hash_password, email) VALUES (6, 'nami.customer', '$2a$10$cEAiE.ry4Z6dgt/QVMQYNe8osRRTj56xXQPWe3v60.mvQQuyq02QC', 'nami.customer@gmail.com');
INSERT INTO customer(id, username, hash_password, email) VALUES (7, 'luffy.customer', '$2a$10$cEAiE.ry4Z6dgt/QVMQYNe8osRRTj56xXQPWe3v60.mvQQuyq02QC', 'luffy.customer@gmail.com');


-- MERCHANT
INSERT INTO merchant(id, username, hash_password, email) VALUES (1, 'john.doe.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'ferdian.ifkarsyah@gmail.com');
INSERT INTO merchant(id, username, hash_password, email) VALUES (2, 'samsung.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'samsung.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email) VALUES (3, 'apple.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'apple.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email) VALUES (4, 'xiaomi.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'xiaomi.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email) VALUES (5, 'adidas.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'adidas.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email) VALUES (6, 'nike.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'nike.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email) VALUES (7, 'gunung.agung.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'gunung.agung.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email) VALUES (8, 'gramedia.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'gramedia.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email) VALUES (9, 'lg.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'lg.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email) VALUES (10, 'panasonic.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'panasonic.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email) VALUES (11, 'toshiba.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'toshiba.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email) VALUES (12, 'acer.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'acer.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email) VALUES (13, 'asus.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'asus.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email) VALUES (14, 'wibu.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'wibu.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email) VALUES (15, 'kpoper.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'kpoper.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email) VALUES (16, 'tejo.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'tejo.merchant@gmail.com');
INSERT INTO merchant(id, username, hash_password, email) VALUES (17, 'yuning.merchant', '$2a$10$MeouQhqHVS.cg85ovzf3Heh.XJRJExp6SZyc7OCjzhLKAGGHonkhy', 'yuning.merchant@gmail.com');


-- ITEM
INSERT INTO item(id, merchant_id, stock, display_name, price, description, image) VALUES (1, 1, 100, 'Dadu', 3000, 'Ini adalah sebuah dadu', 'https://sc01.alicdn.com/kf/H0a4dd4b62de04493a459ea4e900c8edfB.jpg_350x350.jpg');
INSERT INTO item(id, merchant_id, stock, display_name, price, description, image) VALUES (2, 1, 100, 'Pizza', 50000, 'Ini adalah sebuah pizza', 'https://edscyclopedia.com/wp-content/uploads/2016/07/dadu_merah-300x296.png');
INSERT INTO item(id, merchant_id, stock, display_name, price, description, image) VALUES (3, 1, 100, 'Meja', 200000, 'Ini adalah sebuah meja', 'https://cdn.monotaro.id/media/catalog/product/cache/6/image/b5fa40980320eb406ba395dece54e4a8/S/0/S004629955-1.jpg');
INSERT INTO item(id, merchant_id, stock, display_name, price, description, image) VALUES (4, 1, 100, 'Kursi', 3000, 'Ini adalah sebuah kursi', 'https://static.bmdstatic.com/pk/product/medium/5bc7e4e6674f3.jpg');

INSERT INTO item(id, merchant_id, stock, display_name, price, description, image) VALUES (5, 2, 100, 'Galaxi S', 1500000, 'lorem ipsum dolor sit amet', 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/Samsung_Galaxy_S_White.png/160px-Samsung_Galaxy_S_White.png');
INSERT INTO item(id, merchant_id, stock, display_name, price, description, image) VALUES (6, 2, 100, 'Galaxi Note', 2000000, 'lorem ipsum dolor sit amet', 'https://cdn.mos.cms.futurecdn.net/APEMG5VXxpduP6Py8AuGmm-1200-80.jpg');

INSERT INTO item(id, merchant_id, stock, display_name, price, description, image) VALUES (7, 3, 30, 'Macbook Pro', 25000000, 'lorem ipsum dolor sit amet', 'https://9to5mac.com/wp-content/uploads/sites/6/2019/11/16-inch-MacBook-Pro-Top-Features-vs-13-inch-MacBook-Pro.jpg');
INSERT INTO item(id, merchant_id, stock, display_name, price, description, image) VALUES (8, 3, 20, 'Macbook Air', 20000000, 'lorem ipsum dolor sit amet', 'https://www.notebookcheck.net/uploads/tx_nbc2/air13teaser.jpg');
INSERT INTO item(id, merchant_id, stock, display_name, price, description, image) VALUES (9, 3, 10, 'Iphone 10', 12000000, 'lorem ipsum dolor sit amet', 'https://www.static-src.com/wcsstore/Indraprastha/images/catalog/full//104/MTA-1655978/nillkin_nillkin-nature-tpu-casing-for-apple-iphone-x-or-apple-iphone-10---grey_full05.jpg');

INSERT INTO item(id, merchant_id) VALUES (10, 4);
INSERT INTO item(id, merchant_id) VALUES (11, 4);
INSERT INTO item(id, merchant_id) VALUES (12, 4);

INSERT INTO item(id, merchant_id) VALUES (13, 5);
INSERT INTO item(id, merchant_id) VALUES (14, 5);
INSERT INTO item(id, merchant_id) VALUES (15, 5);

INSERT INTO item(id, merchant_id) VALUES (16, 6);
INSERT INTO item(id, merchant_id) VALUES (17, 6);
INSERT INTO item(id, merchant_id) VALUES (18, 6);

INSERT INTO item(id, merchant_id) VALUES (19, 7);
INSERT INTO item(id, merchant_id) VALUES (20, 8);
INSERT INTO item(id, merchant_id) VALUES (21, 9);
INSERT INTO item(id, merchant_id) VALUES (22, 10);
INSERT INTO item(id, merchant_id) VALUES (23, 11);
INSERT INTO item(id, merchant_id) VALUES (24, 12);
INSERT INTO item(id, merchant_id) VALUES (25, 13);
INSERT INTO item(id, merchant_id) VALUES (26, 14);
INSERT INTO item(id, merchant_id) VALUES (27, 15);
INSERT INTO item(id, merchant_id) VALUES (28, 16);
INSERT INTO item(id, merchant_id) VALUES (29, 17);