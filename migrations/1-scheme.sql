CREATE TABLE "customer" (
  "id" BIGSERIAL PRIMARY KEY,
  "username" varchar(255) UNIQUE NOT NULL,
  "hash_password" varchar(255) NOT NULL,
  "email" varchar(255) NOT NULL
);

CREATE TABLE "customer_profile" (
  "customer_id" bigint NOT NULL,
  "display_name" varchar(255),
  "locationID" int
);

CREATE TABLE "customer_bank_account" (
  "id" BIGSERIAL PRIMARY KEY,
  "customer_id" bigint NOT NULL,
  "bank_name" varchar(255),
  "bank_account_number" varchar(255)
);

CREATE TABLE "customer_shopping_cart" (
  "id" BIGSERIAL PRIMARY KEY,
  "customer_id" bigint NOT NULL,
  "item_id" bigint NOT NULL,
  "quantity" int DEFAULT 1
);

CREATE TABLE "customer_transaction_history" (
  "id" BIGSERIAL PRIMARY KEY,
  "customer_id" bigint NOT NULL,
  "transaction_id" bigint NOT NULL,
  "is_success" boolean
);

CREATE TABLE "location" (
  "id" BIGSERIAL PRIMARY KEY,
  "province" varchar(255),
  "city" varchar(255)
);

CREATE TABLE "merchant" (
  "id" BIGSERIAL PRIMARY KEY,
  "username" varchar(255) UNIQUE NOT NULL,
  "hash_password" varchar(255) NOT NULL,
  "email" varchar(255) NOT NULL,
  "admin_approval" boolean,
  "real_wallet" bigint NOT NULL DEFAULT 0
);

CREATE TABLE "merchant_transfer" (
  "id" BIGSERIAL PRIMARY KEY,
  "merchant_id" bigint NOT NULL,
  "merchant_bank_account_id" bigint NOT NULL
);

CREATE TABLE "merchant_profile" (
  "merchant_id" bigint NOT NULL,
  "display_name" varchar(255),
  "locationID" bigint
);

CREATE TABLE "merchant_bank_account" (
  "id" BIGSERIAL PRIMARY KEY,
  "merchant_id" bigint NOT NULL,
  "bank_name" varchar(255),
  "bank_account_number" varchar(255)
);

CREATE TABLE "merchant_request_item" (
  "id" BIGSERIAL PRIMARY KEY,
  "merchant_id" bigint,
  "item_id" bigint,
  "quantity" int DEFAULT 1
);

CREATE TABLE "admin" (
  "id" BIGSERIAL PRIMARY KEY,
  "username" varchar(255) NOT NULL,
  "hash_password" varchar(255) NOT NULL
);

CREATE TABLE "admin_token" (
  "id" BIGSERIAL PRIMARY KEY,
  "token" varchar(255) NOT NULL,
  "used" boolean DEFAULT false
);

CREATE TABLE "item" (
  "id" BIGSERIAL PRIMARY KEY,
  "merchant_id" bigint NOT NULL,
  "stock" int NOT NULL DEFAULT 100,
  "display_name" varchar(255) NOT NULL DEFAULT 'Random name',
  "price" int NOT NULL DEFAULT 1000,
  "description" varchar(255) NOT NULL DEFAULT 'lorem ipsum dolor sit amet',
  "image" varchar(255) NOT NULL DEFAULT 'https://d33v4339jhl8k0.cloudfront.net/docs/assets/5c814e0d2c7d3a0cb9325d1f/images/5c8bc20d2c7d3a154460eb97/file-1CjQ85QAme.jpg'
);

CREATE TABLE "order" (
  "id" BIGSERIAL PRIMARY KEY,
  "item_id" bigint NOT NULL,
  "customer_id" bigint NOT NULL,
  "merchant_id" bigint NOT NULL,
  "order_date" bigint
);

CREATE TABLE "order_detail" (
  "id" BIGSERIAL PRIMARY KEY,
  "order_id" bigint,
  "item_id" bigint,
  "stock" int DEFAULT 1
);

CREATE TABLE "transaction" (
  "id" BIGSERIAL PRIMARY KEY,
  "order_id" bigint NOT NULL,
  "is_paid" boolean DEFAULT false,
  "admin_approval" boolean,
  "transaction_date" bigint
);

ALTER TABLE "customer_profile" ADD FOREIGN KEY ("customer_id") REFERENCES "customer" ("id");

ALTER TABLE "customer_profile" ADD FOREIGN KEY ("locationID") REFERENCES "location" ("id");

ALTER TABLE "customer_bank_account" ADD FOREIGN KEY ("customer_id") REFERENCES "customer" ("id");

ALTER TABLE "customer_shopping_cart" ADD FOREIGN KEY ("customer_id") REFERENCES "customer" ("id");

ALTER TABLE "customer_shopping_cart" ADD FOREIGN KEY ("item_id") REFERENCES "item" ("id");

ALTER TABLE "customer_transaction_history" ADD FOREIGN KEY ("customer_id") REFERENCES "customer" ("id");

ALTER TABLE "customer_transaction_history" ADD FOREIGN KEY ("transaction_id") REFERENCES "transaction" ("id");

ALTER TABLE "merchant_transfer" ADD FOREIGN KEY ("merchant_id") REFERENCES "merchant" ("id");

ALTER TABLE "merchant_transfer" ADD FOREIGN KEY ("merchant_bank_account_id") REFERENCES "merchant_bank_account" ("id");

ALTER TABLE "merchant_profile" ADD FOREIGN KEY ("merchant_id") REFERENCES "merchant" ("id");

ALTER TABLE "merchant_profile" ADD FOREIGN KEY ("locationID") REFERENCES "location" ("id");

ALTER TABLE "merchant_bank_account" ADD FOREIGN KEY ("merchant_id") REFERENCES "merchant" ("id");

ALTER TABLE "merchant_request_item" ADD FOREIGN KEY ("merchant_id") REFERENCES "merchant" ("id");

ALTER TABLE "merchant_request_item" ADD FOREIGN KEY ("item_id") REFERENCES "item" ("id");

ALTER TABLE "item" ADD FOREIGN KEY ("merchant_id") REFERENCES "merchant" ("id");

ALTER TABLE "item_barcode" ADD FOREIGN KEY ("item_id") REFERENCES "item" ("id");

ALTER TABLE "order" ADD FOREIGN KEY ("item_id") REFERENCES "item" ("id");

ALTER TABLE "order" ADD FOREIGN KEY ("customer_id") REFERENCES "customer" ("id");

ALTER TABLE "order" ADD FOREIGN KEY ("merchant_id") REFERENCES "merchant" ("id");

ALTER TABLE "order_detail" ADD FOREIGN KEY ("order_id") REFERENCES "order" ("id");

ALTER TABLE "order_detail" ADD FOREIGN KEY ("item_id") REFERENCES "item" ("id");

ALTER TABLE "transaction" ADD FOREIGN KEY ("order_id") REFERENCES "order" ("id");