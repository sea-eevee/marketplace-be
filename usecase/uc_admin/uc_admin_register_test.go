package uc_admin

import (
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/sea-eevee/marketplace-be/common/token"
	"gitlab.com/sea-eevee/marketplace-be/model"
	"gitlab.com/sea-eevee/marketplace-be/repo/repo_admin/mock"
	"testing"
)

func Test_ucAdmin_Register(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepoAdmin := mock_repo_admin.NewMockContract(ctrl)
	tokenizer := token.NewTokenizer("secret", 10000, 10000)
	u := NewUCAdmin(UCAdminParam{
		RepoAdmin:       mockRepoAdmin,
		RepoMerchant:    nil,
		RepoCustomer:    nil,
		RepoTransaction: nil,
		Tokenizer:       tokenizer,
	})

	sampleToken := "dfjsaklj323"
	sampleUsername := "fjsifjas"
	samplePassword := "fjsasufiwe"
	sampleInsertedID := uint64(5)

	var tts = []struct {
		caseName     string
		params       RegisterParam
		expectations func()
		results      func(resp *RegisterResponse, err error)
	}{
		{
			caseName: "when everything is normal should not return error",
			params:   RegisterParam{RegistrationToken: sampleToken, Username: sampleUsername, Password: samplePassword},
			expectations: func() {
				mockRepoAdmin.EXPECT().Login(sampleUsername).Return(nil, nil)
				mockRepoAdmin.EXPECT().IsAdminTokenExist(sampleToken).Return(true, nil)
				mockRepoAdmin.EXPECT().Register(sampleUsername, gomock.Any()).Return(sampleInsertedID, nil)
			},
			results: func(resp *RegisterResponse, err error) {
				assert.Equal(t, nil, err)
				accessDetail, err := tokenizer.DecodeTokenDetails(resp.Token.AccessToken)
				assert.Equal(t, nil, err)
				assert.Equal(t, sampleInsertedID, accessDetail.UserId)
				assert.Equal(t, model.RoleAdmin, accessDetail.Role)
			},
		},
	}

	for _, tt := range tts {
		t.Log(tt.caseName)

		tt.expectations()

		tt.results(u.Register(&tt.params))
	}
}
