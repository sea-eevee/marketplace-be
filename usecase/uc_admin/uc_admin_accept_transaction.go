package uc_admin

type AcceptTransactionParam struct {
	TransactionID uint64
}
type AcceptTransactionResponse struct{}

type AcceptTransactionUCFunc func(param *AcceptTransactionParam) (*AcceptTransactionResponse, error)

func (u ucAdmin) AcceptTransaction(param *AcceptTransactionParam) (*AcceptTransactionResponse, error) {

	transaction, err := u.repoTransaction.GetTransactionDetail(param.TransactionID)
	if err != nil {
		return nil, err
	}

	item, err := u.repoItem.GetItemDetail(transaction.ID)
	if err != nil {
		return nil, err
	}

	err = u.repoTransaction.AcceptTransaction(
		param.TransactionID,
		transaction.Merchant.ID,
		transaction.Customer.ID,
		item.ID,
		item.Price,
	)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
