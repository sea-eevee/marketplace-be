package uc_admin

import "gitlab.com/sea-eevee/marketplace-be/model"

type ShowAllTransactionParam struct {
}

type ShowAllTransactionResponse struct {
	Transactions []*model.Transaction `json:"transactions"`
}

func (u ucAdmin) ShowAllTransaction(param *ShowAllTransactionParam) (*ShowAllTransactionResponse, error) {
	transactions, err := u.repoTransaction.GetAllTransaction()
	if err != nil {
		return nil, err
	}
	return &ShowAllTransactionResponse{Transactions: transactions}, nil
}
