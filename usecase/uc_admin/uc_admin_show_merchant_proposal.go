package uc_admin

import "gitlab.com/sea-eevee/marketplace-be/model"

type ShowAllMerchantProposalParam struct{}

type ShowAllMerchantProposalResponse struct {
	MerchantProposal []*model.Merchant `json:"merchant_proposal"`
}

func (u ucAdmin) ShowAllMerchantProposal(param *ShowAllMerchantProposalParam) (*ShowAllMerchantProposalResponse, error) {
	panic("implement me")
}
