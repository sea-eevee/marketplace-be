package uc_admin

type AcceptRejectMerchantRegisParam struct {
	MerchantID uint64 `json:"merchant_id"`
	IsApproved bool   `json:"is_approved"`
}

type AcceptRejectMerchantRegisResponse struct{}

type AcceptRejectMerchantRegisUCFunc func(param *AcceptRejectMerchantRegisParam) (*AcceptRejectMerchantRegisResponse, error)

func (u ucAdmin) AcceptOrRejectMerchantRegis(param *AcceptRejectMerchantRegisParam) (*AcceptRejectMerchantRegisResponse, error) {
	err := u.repoAdmin.AcceptOrRejectMerchantRegis(param.MerchantID, param.IsApproved)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
