package uc_admin

import (
	"gitlab.com/sea-eevee/marketplace-be/common/token"
	"gitlab.com/sea-eevee/marketplace-be/repo/repo_admin"
	"gitlab.com/sea-eevee/marketplace-be/repo/repo_customer"
	"gitlab.com/sea-eevee/marketplace-be/repo/repo_item"
	"gitlab.com/sea-eevee/marketplace-be/repo/repo_merchant"
	"gitlab.com/sea-eevee/marketplace-be/repo/repo_transaction"
)

type Contract interface {
	Register(param *RegisterParam) (*RegisterResponse, error)
	Login(param *LoginParam) (*LoginResponse, error)
	AcceptOrRejectMerchantRegis(param *AcceptRejectMerchantRegisParam) (*AcceptRejectMerchantRegisResponse, error)
	AcceptTransaction(param *AcceptTransactionParam) (*AcceptTransactionResponse, error)
	RejectTransaction(param *RejectTransactionParam) (*RejectTransactionResponse, error)
	ShowAllTransaction(param *ShowAllTransactionParam) (*ShowAllTransactionResponse, error)
	ShowAllMerchantProposal(param *ShowAllMerchantProposalParam) (*ShowAllMerchantProposalResponse, error)
}

type ucAdmin struct {
	repoAdmin       repo_admin.Contract
	repoMerchant    repo_merchant.Contract
	repoCustomer    repo_customer.Contract
	repoTransaction repo_transaction.Contract
	repoItem        repo_item.Contract
	tokenizer       token.Contract
}

type UCAdminParam struct {
	RepoAdmin       repo_admin.Contract
	RepoMerchant    repo_merchant.Contract
	RepoCustomer    repo_customer.Contract
	RepoTransaction repo_transaction.Contract
	RepoItem        repo_item.Contract
	Tokenizer       token.Contract
}

func NewUCAdmin(param UCAdminParam) Contract {
	return ucAdmin{
		repoAdmin:       param.RepoAdmin,
		repoCustomer:    param.RepoCustomer,
		repoMerchant:    param.RepoMerchant,
		repoTransaction: param.RepoTransaction,
		repoItem:        param.RepoItem,
		tokenizer:       param.Tokenizer,
	}
}
