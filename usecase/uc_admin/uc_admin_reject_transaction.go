package uc_admin

type RejectTransactionParam struct {
	TransactionID uint64 `json:"transaction_id"`
}
type RejectTransactionResponse struct{}

type RejectTransactionUCFunc func(param *RejectTransactionParam) (*RejectTransactionResponse, error)

func (u ucAdmin) RejectTransaction(param *RejectTransactionParam) (*RejectTransactionResponse, error) {
	//GIVEN I am an admin of the application
	//WHEN I reject the transaction
	//THEN the system should give fail status in the buyer transaction history.

	transaction, err := u.repoTransaction.GetTransactionDetail(param.TransactionID)
	if err != nil {
		return nil, err
	}

	err = u.repoTransaction.RejectTransaction(param.TransactionID, transaction.Customer.ID)
	if err != nil {
		return nil, err
	}

	return nil, nil
}
