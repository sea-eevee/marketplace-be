package uc_admin

import (
	"gitlab.com/sea-eevee/marketplace-be/common/errs"
	"gitlab.com/sea-eevee/marketplace-be/model"
	"golang.org/x/crypto/bcrypt"
)

type RegisterUCFunc func(*RegisterParam) (*RegisterResponse, error)

type RegisterParam struct {
	RegistrationToken string `json:"registration_token"`
	Username          string `json:"username"`
	Password          string `json:"password"`
}

type RegisterResponse struct {
	Token *model.TokenDetails `json:"token"`
}

func (u ucAdmin) Register(param *RegisterParam) (resp *RegisterResponse, err error) {
	_, err = u.repoAdmin.Login(param.Username)
	if err != nil {
		return nil, errs.ErrUsernameExists
	}

	existToken, err := u.repoAdmin.IsAdminTokenExist(param.RegistrationToken)
	if err != nil {
		return nil, errs.ErrAdminTokenNotExists
	}
	if !existToken {
		return nil, errs.ErrBadRequest
	}

	bytes, err := bcrypt.GenerateFromPassword([]byte(param.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	hashedPassword := string(bytes)

	insertedID, err := u.repoAdmin.Register(param.Username, hashedPassword)
	if err != nil {
		return nil, err
	}
	ts, err := u.tokenizer.CreateToken(model.RoleAdmin, insertedID)
	if err != nil {
		return nil, err
	}
	return &RegisterResponse{
		Token: ts,
	}, nil
}
