package uc_admin

import (
	"gitlab.com/sea-eevee/marketplace-be/model"
	"golang.org/x/crypto/bcrypt"
)

type LoginParam struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type LoginResponse struct {
	Token *model.TokenDetails
}

type LoginUCFunc func(param *LoginParam) (*LoginResponse, error)

func (u ucAdmin) Login(param *LoginParam) (*LoginResponse, error) {
	admin, err := u.repoAdmin.Login(param.Username)
	if err != nil {
		return nil, err
	}
	passwordCheck := bcrypt.CompareHashAndPassword([]byte(admin.Password), []byte(param.Password))
	if passwordCheck != nil {
		return nil, passwordCheck
	}
	ts, err := u.tokenizer.CreateToken(model.RoleMerchant, admin.ID)
	if err != nil {
		return nil, err
	}
	return &LoginResponse{Token: ts}, nil
}
