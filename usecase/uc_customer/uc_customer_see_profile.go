package uc_customer

type SeeProfileParam struct {
	CustomerID  uint64
	DisplayName string
}

type SeeProfileResponse struct {
}

type SeeProfileUCFunc func(param *SeeProfileParam) (*SeeProfileResponse, error)

func (u ucCustomer) SeeProfile(param *SeeProfileParam) (*SeeProfileResponse, error) {
	panic("implement me")
}
