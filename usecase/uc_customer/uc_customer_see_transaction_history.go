package uc_customer

import "gitlab.com/sea-eevee/marketplace-be/model"

type SeeTransactionHistoryParam struct {
	CustomerID uint64
}

type SeeTransactionHistoryResponse struct {
	Transactions []*model.Transaction `json:"transactions"`
}

type SeeTransactionHistory func(param *SeeTransactionHistoryParam) (*SeeTransactionHistoryResponse, error)

func (u ucCustomer) SeeTransactionHistory(param *SeeTransactionHistoryParam) (*SeeTransactionHistoryResponse, error) {
	transactions, err := u.repoCustomer.GetTransactionHistory(param.CustomerID)
	if err != nil {
		return nil, err
	}
	return &SeeTransactionHistoryResponse{Transactions: transactions}, nil
}
