package uc_customer

import "gitlab.com/sea-eevee/marketplace-be/model"

type SeeShoppingCartParam struct {
	CustomerID uint64 `json:"customer_id"`
}

type SeeShoppingCartResponse struct {
	Items []*model.Item `json:"items"`
}

type SeeShoppingCartUCFunc func(param *SeeShoppingCartParam) (*SeeShoppingCartResponse, error)

func (u ucCustomer) SeeShoppingCart(param *SeeShoppingCartParam) (*SeeShoppingCartResponse, error) {
	items, err := u.repoCustomer.GetShoppingCart(param.CustomerID)
	if err != nil {
		return nil, err
	}
	return &SeeShoppingCartResponse{Items: items}, nil
}
