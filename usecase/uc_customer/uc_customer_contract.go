package uc_customer

import (
	"gitlab.com/sea-eevee/marketplace-be/common/payload"
	"gitlab.com/sea-eevee/marketplace-be/common/token"
	"gitlab.com/sea-eevee/marketplace-be/repo/repo_customer"
	"gitlab.com/sea-eevee/marketplace-be/repo/repo_merchant"
)

type Contract interface {
	Register(*payload.RegisterRequest) (*payload.RegisterResponse, error)
	Login(*LoginParam) (*LoginResponse, error)
	SeeProfile(*SeeProfileParam) (*SeeProfileResponse, error)
	UpdateProfile(*UpdateProfileParam) (*UpdateProfileResponse, error)
	SeeAllMerchant(*SeeAllMerchantParam) (*SeeAllMerchantResponse, error)
	SeeMerchantItems(*SeeMerchantItemsParam) (*SeeMerchantItemsResponse, error)
	AddItemToCart(*AddItemToCartParam) (*AddItemToCartResponse, error)
	SeeShoppingCart(*SeeShoppingCartParam) (*SeeShoppingCartResponse, error)
	PayMerchantForItem(*PayMerchantForItemParam) (*PayMerchantForItemResponse, error)
	SeeTransactionHistory(*SeeTransactionHistoryParam) (*SeeTransactionHistoryResponse, error)
}

type ucCustomer struct {
	repoMerchant repo_merchant.Contract
	repoCustomer repo_customer.Contract
	tokenizer    token.Contract
}

func NewUCCustomer(repoCustomer repo_customer.Contract, repoMerchant repo_merchant.Contract, tokenizer token.Contract) Contract {
	return ucCustomer{repoCustomer: repoCustomer, repoMerchant: repoMerchant, tokenizer: tokenizer}
}
