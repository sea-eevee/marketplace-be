package uc_customer

import (
	"gitlab.com/sea-eevee/marketplace-be/common/errs"
	"gitlab.com/sea-eevee/marketplace-be/common/payload"
	"gitlab.com/sea-eevee/marketplace-be/model"
	"golang.org/x/crypto/bcrypt"
)

type RegisterParam struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Email    string `json:"email"`
}

type RegisterUCFunc func(param *payload.RegisterRequest) (*payload.RegisterResponse, error)

func (u ucCustomer) Register(param *payload.RegisterRequest) (*payload.RegisterResponse, error) {
	_, err := u.repoCustomer.GetByUsername(param.Username)
	if err != nil {
		return nil, errs.ErrUsernameExists
	}

	bytes, err := bcrypt.GenerateFromPassword([]byte(param.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	param.Password = string(bytes)

	insertedID, err := u.repoCustomer.Create(param.Username, param.Password, param.Email)
	if err != nil {
		return nil, err
	}

	ts, err := u.tokenizer.CreateToken(model.RoleCustomer, uint64(insertedID))
	if err != nil {
		return nil, err
	}

	return &payload.RegisterResponse{Token: ts}, err
}
