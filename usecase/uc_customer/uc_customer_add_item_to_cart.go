package uc_customer

type AddItemToCartParam struct {
	CustomerID uint64 `json:"customerID"`
	ItemID     uint64 `json:"item_id"`
}

type AddItemToCartResponse struct {
}

type AddItemToCartUCFunc func(*AddItemToCartParam) (*AddItemToCartResponse, error)

func (u ucCustomer) AddItemToCart(param *AddItemToCartParam) (*AddItemToCartResponse, error) {
	err := u.repoCustomer.AddToCart(param.CustomerID, param.ItemID)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
