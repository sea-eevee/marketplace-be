package uc_customer

type UpdateProfileParam struct {
	CustomerID  uint64
	DisplayName string
}

type UpdateProfileResponse struct {
}

type UpdateProfileUCFunc func(param *UpdateProfileParam) (*UpdateProfileResponse, error)

func (u ucCustomer) UpdateProfile(param *UpdateProfileParam) (*UpdateProfileResponse, error) {
	panic("implement me")
}
