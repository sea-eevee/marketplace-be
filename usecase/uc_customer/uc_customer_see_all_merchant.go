package uc_customer

import "gitlab.com/sea-eevee/marketplace-be/model"

type SeeAllMerchantParam struct {
}

type SeeAllMerchantResponse struct {
	Merchants []*model.Merchant `json:"merchants"`
}

type SeeAllMerchantUCFunc func(param *SeeAllMerchantParam) (*SeeAllMerchantResponse, error)

func (u ucCustomer) SeeAllMerchant(param *SeeAllMerchantParam) (*SeeAllMerchantResponse, error) {
	merchants, err := u.repoMerchant.GetAllMerchantDisplay()
	if err != nil {
		return nil, err
	}
	return &SeeAllMerchantResponse{Merchants: merchants}, nil
}
