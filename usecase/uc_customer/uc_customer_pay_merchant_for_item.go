package uc_customer

type PayMerchantForItemParam struct {
	ShoppingCartID uint64 `json:"shopping_cart_id"`
	BankName       string `json:"bank_name"`
	BankAccount    string `json:"bank_account"`
}

type PayMerchantForItemResponse struct {
}

type PayMerchantForItemUCFunc func(param *PayMerchantForItemParam) (*PayMerchantForItemResponse, error)

func (u ucCustomer) PayMerchantForItem(param *PayMerchantForItemParam) (*PayMerchantForItemResponse, error) {
	//GIVEN I am a registered customer
	//WHEN I have selected the items from the merchant page
	//THEN I can pay them with money transfer by filling in the form that contains bank name
	//and bank account number (dont use real life information!))

	//GIVEN I am a registered customer
	//WHEN I have selected the items from the merchant page
	//THEN I can pay them with money transfer by filling in the form that contains bank name
	//and bank account number (dont use real life information!))
	panic("implement me")
}
