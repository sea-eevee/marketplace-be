package uc_customer

import "gitlab.com/sea-eevee/marketplace-be/model"

type SeeMerchantItemsParam struct {
	MerchantID uint64 `json:"merchant_id"`
}

type SeeMerchantItemsResponse struct {
	Items []*model.Item `json:"items"`
}

type SeeMerchantItemsUCFunc func(param *SeeMerchantItemsParam) (*SeeMerchantItemsResponse, error)

func (u ucCustomer) SeeMerchantItems(param *SeeMerchantItemsParam) (*SeeMerchantItemsResponse, error) {
	items, err := u.repoMerchant.GetMerchantItems(param.MerchantID)
	if err != nil {
		return nil, err
	}
	return &SeeMerchantItemsResponse{Items: items}, nil
}
