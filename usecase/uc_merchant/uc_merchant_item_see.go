package uc_merchant

import "gitlab.com/sea-eevee/marketplace-be/model"

func (U ucMerchant) SeeMyItems(merchantID int) ([]*model.Item, error) {
	panic("implement me")
}
