package uc_merchant

import (
	"gitlab.com/sea-eevee/marketplace-be/common/payload"
	"gitlab.com/sea-eevee/marketplace-be/model"
	"golang.org/x/crypto/bcrypt"
)

type LoginUCFunc func(*payload.LoginRequest) (*payload.LoginResponse, error)

func (u ucMerchant) Login(data *payload.LoginRequest) (response *payload.LoginResponse, err error) {
	merchant, err := u.repoMerchant.Login(data.Username)
	if err != nil {
		return nil, err
	}
	passwordCheck := bcrypt.CompareHashAndPassword([]byte(merchant.Password), []byte(data.Password))
	if passwordCheck != nil {
		return nil, passwordCheck
	}
	ts, err := u.tokenizer.CreateToken(model.RoleMerchant, uint64(merchant.ID))
	if err != nil {
		return nil, err
	}
	return &payload.LoginResponse{Token: ts, User: merchant}, nil
}
