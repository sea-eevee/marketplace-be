package uc_merchant

import (
	"gitlab.com/sea-eevee/marketplace-be/common/payload"
	"gitlab.com/sea-eevee/marketplace-be/common/token"
	"gitlab.com/sea-eevee/marketplace-be/model"
	"gitlab.com/sea-eevee/marketplace-be/repo/repo_merchant"
)

type Contract interface {
	Register(data *payload.RegisterRequest) (*payload.RegisterResponse, error)
	Login(data *payload.LoginRequest) (response *payload.LoginResponse, err error)
	UpdateProfile(updated *model.Merchant) error
	AddItem(merchantID int, item *model.Item) (insertedID int, err error)
	UpdateItem(merchantID int, item *model.Item) error
	RemoveItem(merchantID int, item *model.Item) error
	SeeMyItems(merchantID int) ([]*model.Item, error)
	SeeTransferHistory(merchantID int) ([]*model.Transaction, error)
}

type ucMerchant struct {
	repoMerchant repo_merchant.Contract
	tokenizer    token.Contract
}

func NewUCMerchant(repoMerchant repo_merchant.Contract, tokenizer token.Contract) Contract {
	return &ucMerchant{
		repoMerchant: repoMerchant,
		tokenizer:    tokenizer,
	}
}
