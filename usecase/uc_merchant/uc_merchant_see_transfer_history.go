package uc_merchant

import "gitlab.com/sea-eevee/marketplace-be/model"

func (U ucMerchant) SeeTransferHistory(merchantID int) ([]*model.Transaction, error) {
	panic("implement me")
}
