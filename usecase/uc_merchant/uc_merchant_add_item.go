package uc_merchant

import (
	"gitlab.com/sea-eevee/marketplace-be/model"
)

type AddItemUCFunc func(merchantID int, item *model.Item) (insertedID int, err error)

func (u ucMerchant) AddItem(merchantID int, item *model.Item) (insertedID int, err error) {
	insertedID, err = u.repoMerchant.AddItem(merchantID, item)
	if err != nil {
		return 0, err
	}
	return insertedID, err
}
