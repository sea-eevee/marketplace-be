package uc_merchant

import (
	"gitlab.com/sea-eevee/marketplace-be/common/payload"
	"gitlab.com/sea-eevee/marketplace-be/model"
	"golang.org/x/crypto/bcrypt"
)

type RegisterUCFunc func(*payload.RegisterRequest) (*payload.RegisterResponse, error)

func (u ucMerchant) Register(data *payload.RegisterRequest) (*payload.RegisterResponse, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(data.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	data.Password = string(bytes)

	insertedID, err := u.repoMerchant.Create(data)
	if err != nil {
		return nil, err
	}
	ts, err := u.tokenizer.CreateToken(model.RoleMerchant, uint64(insertedID))
	if err != nil {
		return nil, err
	}

	return &payload.RegisterResponse{Token: ts}, err

}
