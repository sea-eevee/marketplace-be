package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/sea-eevee/marketplace-be/api/api_customer"
	"gitlab.com/sea-eevee/marketplace-be/common/token"
	"gitlab.com/sea-eevee/marketplace-be/usecase/uc_customer"
	"net/http"
)

func ApplyRouterCustomer(r *mux.Router, ucCustomer uc_customer.Contract, tokenizer token.Contract) *mux.Router {

	customer := r.PathPrefix("/customer").Subrouter()
	{
		customer.
			Methods(http.MethodPost).
			Path("/register").
			Handler(api_customer.Register(ucCustomer.Register))

		customer.
			Methods(http.MethodGet).
			Path("/login").
			Handler(api_customer.Login(ucCustomer.Login))

		customer.
			Methods(http.MethodGet).
			Path("/profile").
			Handler(tokenizer.MiddlewareAuth(api_customer.SeeProfile(ucCustomer.SeeProfile)))

		customer.
			Methods(http.MethodPut).
			Path("/profile").
			Handler(tokenizer.MiddlewareAuth(api_customer.UpdateProfile(ucCustomer.UpdateProfile)))

		customer.
			Methods(http.MethodGet).
			Path("/merchant").
			Handler(tokenizer.MiddlewareAuth(api_customer.SeeAllMerchant(ucCustomer.SeeAllMerchant)))

		customer.
			Methods(http.MethodGet).
			Path(fmt.Sprintf("/merchant/{%s:[0-9]+}", api_customer.URLKeyMerchantID)).
			Handler(tokenizer.MiddlewareAuth(api_customer.SeeMerchantItems(ucCustomer.SeeMerchantItems)))

		customer.
			Methods(http.MethodPost).
			Path("/addtocart").
			Handler(tokenizer.MiddlewareAuth(api_customer.AddItemToCart(ucCustomer.AddItemToCart)))

		customer.
			Methods(http.MethodGet).
			Path("/cart").
			Handler(tokenizer.MiddlewareAuth(api_customer.SeeShoppingCart(ucCustomer.SeeShoppingCart)))

		customer.
			Methods(http.MethodGet).
			Path("/transactionhistory").
			Handler(tokenizer.MiddlewareAuth(api_customer.SeeTransactionHistory(ucCustomer.SeeTransactionHistory)))

	}
	return r
}
