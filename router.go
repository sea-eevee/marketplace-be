package main

import (
	"github.com/gorilla/mux"
	"gitlab.com/sea-eevee/marketplace-be/common/token"
)

func NewRouter(d domain, tokenizer token.Contract) *mux.Router {
	root := mux.NewRouter()

	ApplyRouterCustomer(root, d.ucCustomer, tokenizer)
	ApplyRouterMerchant(root, d.ucMerchant, tokenizer)
	ApplyRouterAdmin(root, d.ucAdmin, tokenizer)

	return root
}
